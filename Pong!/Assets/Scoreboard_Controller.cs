﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard_Controller : MonoBehaviour
{

    public static Scoreboard_Controller instance;

    public Text PlayerOneScoreText;
    public Text PlayerTwoScoreText;

    public int PlayerOneScore;
    public int PlayerTwoScore;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        PlayerOneScore = PlayerTwoScore = 0;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GivePlayerOneAPoint()
    {
        PlayerOneScore += 1;

        PlayerOneScoreText.text = PlayerOneScore.ToString ();
    }

    public void GivePlayerTwoAPoint ()
    {
        PlayerTwoScore += 1;
        PlayerTwoScoreText.text = PlayerTwoScore.ToString ();
    }
}
