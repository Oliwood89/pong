﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    Rigidbody2D rb;

    [SerializeField]
    float speed;

    float radius;
    Vector2 direction;
    float change = 0;
    bool isRestart;
    float timeActual;
    float SaveSpeed;


    // Start is called before the first frame update
    void Start()
    {

        SaveSpeed = speed;

        rb = GetComponent<Rigidbody2D> ();

        rb.velocity = new Vector2(1f, 0f);

        timeActual = 0;
        direction = Vector2.one.normalized; // Direction is (1,1) normalized
        radius = transform.localScale.x / 2; //Half the width
    }

    // Update is called once per frame
    void Update()
    {
        if(!isRestart)
        {
            transform.Translate(direction * speed * Time.deltaTime);

            // Bounce off top and bottom
            if (transform.position.y < GameManager.bottomLeft.y + radius && direction.y < 0)
            {
                direction.y = -direction.y;
            }
            if (transform.position.y > GameManager.topRight.y - radius && direction.y > 0)
            {
                direction.y = -direction.y;
            }

            // Game over
            if (transform.position.x < GameManager.bottomLeft.x + radius && direction.x < 0)
            {
                
                Debug.Log("Right player wins !!");
                ResetBall();
                speed = SaveSpeed;

                //Give Player_2 a point
                Scoreboard_Controller.instance.GivePlayerTwoAPoint();
            }
            if (transform.position.x > GameManager.topRight.x - radius && direction.x > 0)
            {
                
                Debug.Log("Left player wins !!");
                ResetBall();
                speed = SaveSpeed;

                //Give Player_1 a point
                Scoreboard_Controller.instance.GivePlayerOneAPoint();
            }
        }
        else
        {
            if(Time.fixedTime - timeActual >= 1)
            {
                isRestart = false;
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Paddle")
        {
            bool isRight = other.GetComponent<Paddle>().isRight;

            //If hitting right paddle and moving right, flip direction
            if (isRight == true && direction.x > 0)
            {
                direction.x = -direction.x;
                speed += 1f;
            }
            //If hitting left paddle and moving right, flip direction
            if (isRight == false && direction.x < 0)
            {
                direction.x = -direction.x;
                speed += 1f;
            }
             //Si tu veux faire en sorte que la balle change de sens selon la ou elle tape 
            if (other.transform.position.y + (other.transform.localScale.y / 2) < other.transform.localScale.y / 2)
            {
                direction.y = -direction.y;
            }
        }
    }
    void ResetBall()
    {
        transform.position = new Vector2(0, 0);
        isRestart = true;
        timeActual = Time.fixedTime;
    }
}
