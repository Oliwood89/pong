﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOUND_MENU : MonoBehaviour
{
    AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void onClickSound() {
        source.Play(0);
    }
    public void onClickExit() {
        Application.Quit();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
